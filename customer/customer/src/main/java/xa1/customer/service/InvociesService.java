package xa1.customer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa1.customer.model.Invocies;
import xa1.customer.respository.InvociesRepositories;

import java.util.List;

@Service
public class InvociesService {
    @Autowired
    private InvociesRepositories invociesRepositories;

    public List<Invocies> getAllInvoice(){
        return this.invociesRepositories.findAll();
    }

    public Invocies getInvoiceById(Long id){
        return this.invociesRepositories.findById(id).orElse(null);
    }

    public void saveInvoice(Invocies invocies){
        this.invociesRepositories.save(invocies);
    }

    public void deleteInvoice(Long id){
        this.invociesRepositories.deleteById(id);
    }
}
