package xa1.customer.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import xa1.customer.model.Booking;
import xa1.customer.model.BookingDTO;
import xa1.customer.respository.BookingRepositories;

import java.util.List;

@Service
public class BookingService {
    private static final String TOPIC = "booking";
    @Autowired
    private BookingRepositories bookingRepositories;

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public List<Booking> getAllBooking(){
        return this.bookingRepositories.findAll();
    }

    public Booking getBookingById(Long id){
        return this.bookingRepositories.findById(id).orElse(null);
    }

    public void saveBooking(Booking booking){
        this.bookingRepositories.save(booking);
    }

    public void deleteBooking(Long id){
        this.bookingRepositories.deleteById(id);
    }

    public BookingDTO convertToDTO(Booking booking){
        ModelMapper modelMapper = new ModelMapper();
        BookingDTO bookingDTO = modelMapper.map(booking, BookingDTO.class);
        return bookingDTO;
    }

    public void sendData(String data){
        this.kafkaTemplate.send(TOPIC, data);
    }
}
