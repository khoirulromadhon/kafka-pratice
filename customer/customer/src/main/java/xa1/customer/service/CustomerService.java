package xa1.customer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa1.customer.model.Customer;
import xa1.customer.respository.CustomerRepositories;

import java.util.List;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepositories customerRepositories;

    public List<Customer> getAllCustomer(){
        return this.customerRepositories.findAll();
    }

    public Customer getCustomerById(Long id){
        return this.customerRepositories.findById(id).orElse(null);
    }

    public void saveCustomer(Customer customer){
        this.customerRepositories.save(customer);
    }

    public void deleteCustomer(Long id){
        this.customerRepositories.deleteById(id);
    }
}
