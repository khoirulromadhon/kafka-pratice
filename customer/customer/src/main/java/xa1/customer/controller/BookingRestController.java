package xa1.customer.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa1.customer.model.Booking;
import xa1.customer.service.BookingService;

import java.util.*;

@RestController
@RequestMapping("/rest_booking")
@CrossOrigin("*")
public class BookingRestController {
    @Autowired
    private BookingService bookingService;

    @GetMapping("booking")
    public ResponseEntity<List<Booking>> getAllBooking(){
        try {
            List<Booking> bookingList = this.bookingService.getAllBooking();
            return new ResponseEntity<>(bookingList, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("booking/{id}")
    public ResponseEntity<?> getBookingById(@PathVariable("id") Long id){
        try {
            Booking booking = this.bookingService.getBookingById(id);
            return new ResponseEntity<>(booking, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("booking")
    public ResponseEntity<Booking> saveBooking(@RequestBody Booking booking) {
        booking.setBooking_date(new Date());
        try {
            this.bookingService.saveBooking(booking);

            ObjectMapper obj = new ObjectMapper();

            String jsonString = obj.writeValueAsString(this.bookingService.convertToDTO(booking));
            this.bookingService.sendData(jsonString);

            return new ResponseEntity<Booking>(booking, HttpStatus.OK);
        }
        catch (Exception exception){
            Map<String, String> result = new HashMap<>();
            result.put("Message", exception.getMessage());
            result.put("Data", null);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("booking/{id}")
    public ResponseEntity<?> updateBooking(@RequestBody Booking booking, @PathVariable("id") Long id){
        try {
            booking.setId(id);
            this.bookingService.saveBooking(booking);
            Map<String, Object> result = new HashMap<>();
            String message = "Update Success !!!!!";
            result.put("Message", message);
            result.put("Data", booking);
            return new ResponseEntity<>(booking, HttpStatus.OK);
        }
        catch (Exception exception) {
            Map<String, String> result = new HashMap<>();
            result.put("Message", exception.getMessage());
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("booking/{id}")
    public ResponseEntity<?> deleteBooking(@PathVariable("id") Long id){
        try {
            Booking booking = this.bookingService.getBookingById(id);

            if (booking != null){
                this.bookingService.deleteBooking(id);
                return ResponseEntity.status(HttpStatus.OK).body("Booking Deleted !!!!!");
            }
            else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Booking Not Found !!!!!");
            }
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
