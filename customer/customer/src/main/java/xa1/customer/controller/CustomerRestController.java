package xa1.customer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa1.customer.model.Customer;
import xa1.customer.service.CustomerService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/rest_customer")
@CrossOrigin("*")
public class CustomerRestController {
    @Autowired
    private CustomerService customerService;

    @GetMapping("customer")
    public ResponseEntity<List<Customer>> getAllCustomer(){
        try {
            List<Customer> customers = this.customerService.getAllCustomer();
            return new ResponseEntity<>(customers, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("customer/{id}")
    public ResponseEntity<?> getCustomerById(@PathVariable("id") Long id){
        try {
            Customer customer = this.customerService.getCustomerById(id);
            return new ResponseEntity<>(customer, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("customer")
    public ResponseEntity<Customer> saveCustomer(@RequestBody Customer customer){
        try {
            this.customerService.saveCustomer(customer);
            return new ResponseEntity<Customer>(customer, HttpStatus.OK);
        }
        catch (Exception exception){
            Map<String, String> result = new HashMap<>();
            result.put("Message", exception.getMessage());
            result.put("Data", null);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("customer/{id}")
    public ResponseEntity<?> updateCustomer(@RequestBody Customer customer, @PathVariable("id") Long id){
        try {
            customer.setId(id);
            this.customerService.saveCustomer(customer);
            Map<String, Object> result = new HashMap<>();
            String message = "Update Success !!!!!!";
            result.put("Message", message);
            result.put("Data", customer);
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        catch (Exception exception){
            Map<String, String> result = new HashMap<>();
            result.put("Message", exception.getMessage());
            result.put("Data", null);
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("customer/{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable("id") Long id){
        try {
            Customer customer = this.customerService.getCustomerById(id);

            if (customer != null){
                this.customerService.deleteCustomer(id);
                return ResponseEntity.status(HttpStatus.OK).body("Customer Deleted !!!!!");
            }
            else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Customer not Found !!!!!");
            }
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
