package xa1.customer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa1.customer.model.Invocies;
import xa1.customer.service.InvociesService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/rest_invoice")
@CrossOrigin("*")
public class InvociesRestController {
    @Autowired
    private InvociesService invociesService;

    @GetMapping("invoice")
    public ResponseEntity<List<Invocies>> getAllInvoice(){
        try {
            List<Invocies> invociesList = this.invociesService.getAllInvoice();
            return new ResponseEntity<>(invociesList, HttpStatus.OK);
        }
        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("invoice/{id}")
    public ResponseEntity<?> getInvoiceById(@PathVariable("id") Long id){
        try {
            Invocies invocies = this.invociesService.getInvoiceById(id);
            return new ResponseEntity<>(invocies, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("invoice")
    public ResponseEntity<Invocies> saveInvoice(@RequestBody Invocies invocies){
        try {
            this.invociesService.saveInvoice(invocies);
            return new ResponseEntity<Invocies>(invocies, HttpStatus.OK);
        }
        catch (Exception exception) {
            Map<String, String> result = new HashMap<>();
            result.put("Message", exception.getMessage());
            result.put("Data", null);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("invoice/{id}")
    public ResponseEntity<?> updateInvoice(@RequestBody Invocies invocies, @PathVariable("id") Long id){
        try {
            invocies.setId(id);
            this.invociesService.saveInvoice(invocies);
            Map<String, Object> result = new HashMap<>();
            String message = "Update Success !!!!!";
            result.put("Message", message);
            result.put("Data", invocies);
            return new ResponseEntity<>(invocies, HttpStatus.OK);
        }
        catch (Exception exception){
            Map<String, String> result = new HashMap<>();
            result.put("Message", exception.getMessage());
            result.put("Data", null);
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("invoice/{id}")
    public ResponseEntity<?> deleteInvoice(@PathVariable("id") Long id){
        try {
            Invocies invocies = this.invociesService.getInvoiceById(id);

            if (invocies != null){
                this.invociesService.deleteInvoice(id);
                return ResponseEntity.status(HttpStatus.OK).body("Invoice Deleted !!!!!");
            }
            else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Invoice Not Found !!!!!");
            }
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
