package xa1.customer.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import xa1.customer.model.Invocies;

public interface InvociesRepositories extends JpaRepository<Invocies, Long> {
}
