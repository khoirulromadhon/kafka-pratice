package xa1.customer.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import xa1.customer.model.Booking;

public interface BookingRepositories extends JpaRepository<Booking, Long> {
}
