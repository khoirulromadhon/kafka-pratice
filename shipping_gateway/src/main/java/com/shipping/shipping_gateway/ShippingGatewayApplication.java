package com.shipping.shipping_gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShippingGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShippingGatewayApplication.class, args);
	}

}
