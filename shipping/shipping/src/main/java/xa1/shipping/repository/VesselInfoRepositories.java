package xa1.shipping.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import xa1.shipping.model.VesselInfo;

public interface VesselInfoRepositories extends JpaRepository<VesselInfo, Long> {
}
