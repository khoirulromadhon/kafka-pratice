package xa1.shipping.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import xa1.shipping.model.CargoInfo;

public interface CargoInfoRepositories extends JpaRepository<CargoInfo, Long> {
}
