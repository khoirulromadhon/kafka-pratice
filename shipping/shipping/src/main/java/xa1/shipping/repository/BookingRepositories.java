package xa1.shipping.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import xa1.shipping.model.Booking;

public interface BookingRepositories extends JpaRepository<Booking, Long> {
}
