package xa1.shipping.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import xa1.shipping.model.Booking;
import xa1.shipping.repository.BookingRepositories;

@Service
public class BookingService {
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired private BookingRepositories bookingRepositories;

    @KafkaListener(topics = "booking", groupId = "group_booking")
    public void consumberBooking(String data) throws JsonProcessingException{
        Booking booking = objectMapper.readValue(data, Booking.class);
        this.bookingRepositories.save(booking);
    }
}
