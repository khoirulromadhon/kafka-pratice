package xa1.shipping.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa1.shipping.model.VesselInfo;
import xa1.shipping.repository.VesselInfoRepositories;

import java.util.List;

@Service
public class VesselInfoService {
    @Autowired
    private VesselInfoRepositories vesselInfoRepositories;

    public List<VesselInfo> getAllVesselInfo(){
        return this.vesselInfoRepositories.findAll();
    }

    public VesselInfo getVesselInfoById(Long id){
        return this.vesselInfoRepositories.findById(id).orElse(null);
    }

    public void saveVesselInfo(VesselInfo vesselInfo){
        this.vesselInfoRepositories.save(vesselInfo);
    }

    public void deleteVesselInfo(Long id){
        this.vesselInfoRepositories.deleteById(id);
    }
}
