package xa1.shipping.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa1.shipping.model.CargoInfo;
import xa1.shipping.repository.CargoInfoRepositories;

import java.util.List;

@Service
public class CargoInfoService {
    @Autowired
    private CargoInfoRepositories cargoInfoRepositories;

    public List<CargoInfo> getAllCargoInfo(){
        return this.cargoInfoRepositories.findAll();
    }

    public CargoInfo getCargoInfoById(Long id){
        return this.cargoInfoRepositories.findById(id).orElse(null);
    }

    public void saveCargoInfo(CargoInfo cargoInfo){
        this.cargoInfoRepositories.save(cargoInfo);
    }

    public void deleteCargoInfo(Long id){
        this.cargoInfoRepositories.deleteById(id);
    }
}
