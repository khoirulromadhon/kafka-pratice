package xa1.shipping.model;

import jakarta.persistence.*;

@Entity
@Table(name = "vessel_infos")
public class VesselInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "cargo_id")
    private Long cargo_id;

    @ManyToOne
    @JoinColumn(name = "cargo_id", insertable = false, updatable = false)
    public CargoInfo cargoInfo;

    @Column(name = "vessel_name")
    private String vessel_name;

    @Column(name = "vessel_capacity")
    private int vessel_capacity;

    @Column(name = "vessel_speed")
    private int vessel_speed;

    @Column(name = "vessel_length")
    private int vessel_length;

    @Column(name = "vessel_breadth")
    private int vessel_breadth;

    @Column(name = "vessel_type")
    private String vessel_type;

    @Column(name = "vessel_status")
    private String vessel_status;

    @Column(name = "gross_tonnage")
    private int gross_tonnage;

    @Column(name = "deadweight_tonnage")
    private int deadweight_tonnage;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getCargo_id() {
        return cargo_id;
    }

    public void setCargo_id(Long cargo_id) {
        this.cargo_id = cargo_id;
    }

    public CargoInfo getCargoInfo() {
        return cargoInfo;
    }

    public void setCargoInfo(CargoInfo cargoInfo) {
        this.cargoInfo = cargoInfo;
    }

    public String getVessel_name() {
        return vessel_name;
    }

    public void setVessel_name(String vessel_name) {
        this.vessel_name = vessel_name;
    }

    public int getVessel_capacity() {
        return vessel_capacity;
    }

    public void setVessel_capacity(int vessel_capacity) {
        this.vessel_capacity = vessel_capacity;
    }

    public int getVessel_speed() {
        return vessel_speed;
    }

    public void setVessel_speed(int vessel_speed) {
        this.vessel_speed = vessel_speed;
    }

    public int getVessel_length() {
        return vessel_length;
    }

    public void setVessel_length(int vessel_length) {
        this.vessel_length = vessel_length;
    }

    public int getVessel_breadth() {
        return vessel_breadth;
    }

    public void setVessel_breadth(int vessel_breadth) {
        this.vessel_breadth = vessel_breadth;
    }

    public String getVessel_type() {
        return vessel_type;
    }

    public void setVessel_type(String vessel_type) {
        this.vessel_type = vessel_type;
    }

    public String getVessel_status() {
        return vessel_status;
    }

    public void setVessel_status(String vessel_status) {
        this.vessel_status = vessel_status;
    }

    public int getGross_tonnage() {
        return gross_tonnage;
    }

    public void setGross_tonnage(int gross_tonnage) {
        this.gross_tonnage = gross_tonnage;
    }

    public int getDeadweight_tonnage() {
        return deadweight_tonnage;
    }

    public void setDeadweight_tonnage(int deadweight_tonnage) {
        this.deadweight_tonnage = deadweight_tonnage;
    }
}
