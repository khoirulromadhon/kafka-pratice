package xa1.shipping.model;

import jakarta.persistence.*;

@Entity
@Table(name = "cargo_infos")
public class CargoInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "booking_id")
    private Long booking_id;

    @ManyToOne
    @JoinColumn(name = "booking_id", insertable = false, updatable = false)
    public Booking booking;

    @Column(name = "cargo_type")
    private String cargo_type;

    @Column(name = "cargo_description")
    private String cargo_description;

    @Column(name = "cargo_weight")
    private int cargo_weight;

    @Column(name = "cargo_volume")
    private int cargo_volume;

    @Column(name = "hazardous_or_fragile")
    private Boolean hazardous_or_fragile;

    @Column(name = "quantity")
    private int quantity;

    @Column(name = "unit_price")
    private long unit_price;

    @Column(name = "cargo_price")
    private long cargo_price;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(long booking_id) {
        this.booking_id = booking_id;
    }

    public String getCargo_type() {
        return cargo_type;
    }

    public void setCargo_type(String cargo_type) {
        this.cargo_type = cargo_type;
    }

    public String getCargo_description() {
        return cargo_description;
    }

    public void setCargo_description(String cargo_description) {
        this.cargo_description = cargo_description;
    }

    public int getCargo_weight() {
        return cargo_weight;
    }

    public void setCargo_weight(int cargo_weight) {
        this.cargo_weight = cargo_weight;
    }

    public int getCargo_volume() {
        return cargo_volume;
    }

    public void setCargo_volume(int cargo_volume) {
        this.cargo_volume = cargo_volume;
    }

    public Boolean getHazardous_or_fragile() {
        return hazardous_or_fragile;
    }

    public void setHazardous_or_fragile(Boolean hazardous_or_fragile) {
        this.hazardous_or_fragile = hazardous_or_fragile;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public long getUnit_price() {
        return unit_price;
    }

    public void setUnit_price(long unit_price) {
        this.unit_price = unit_price;
    }

    public long getCargo_price() {
        return cargo_price;
    }

    public void setCargo_price(long cargo_price) {
        this.cargo_price = cargo_price;
    }
}
