package xa1.shipping.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa1.shipping.model.VesselInfo;
import xa1.shipping.service.VesselInfoService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/rest_vessel")
@CrossOrigin("*")
public class VesselInfoRestController {
    @Autowired
    private VesselInfoService vesselInfoService;

    @GetMapping("vessel_info")
    public ResponseEntity<List<VesselInfo>> getAllVesselInfo(){
        try {
            List<VesselInfo> vesselInfoList = this.vesselInfoService.getAllVesselInfo();
            return new ResponseEntity<>(vesselInfoList, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("vessel_info/{id}")
    public ResponseEntity<?> getVesselInfoById(@PathVariable("id") Long id){
        try {
            VesselInfo vesselInfo = this.vesselInfoService.getVesselInfoById(id);
            return new  ResponseEntity<>(vesselInfo, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("vessel_info")
    public ResponseEntity<VesselInfo> saveVesselInfo(@RequestBody VesselInfo vesselInfo) {
        try {
            this.vesselInfoService.saveVesselInfo(vesselInfo);
            return new ResponseEntity<VesselInfo>(vesselInfo, HttpStatus.OK);
        }
        catch (Exception exception){
            Map<String, String> result = new HashMap<>();
            result.put("Message", exception.getMessage());
            result.put("Data", null);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("vessel_info/{id}")
    public ResponseEntity<?> updateVesselInfo(@RequestBody VesselInfo vesselInfo, @PathVariable("id") Long id){
        try {
            vesselInfo.setId(id);
            this.vesselInfoService.saveVesselInfo(vesselInfo);
            Map<String, Object> result = new HashMap<>();
            String message = "Update Data Success !!!!!";
            result.put("Message", message);
            result.put("Data", vesselInfo);
            return new ResponseEntity<>(vesselInfo, HttpStatus.OK);
        }
        catch (Exception exception){
            Map<String, String> result = new HashMap<>();
            result.put("Message", exception.getMessage());
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("vessel_info/{id}")
    public ResponseEntity<?> deleteVesselInfo(@PathVariable("id") Long id){
        try {
            VesselInfo vesselInfo = this.vesselInfoService.getVesselInfoById(id);

            if (vesselInfo != null) {
                this.vesselInfoService.deleteVesselInfo(id);
                return ResponseEntity.status(HttpStatus.OK).body("Vessel Info Deleted !!!!!");
            }
            else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Vessel Info Not Found !!!!!");
            }
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
