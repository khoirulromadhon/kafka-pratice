package xa1.shipping.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa1.shipping.model.CargoInfo;
import xa1.shipping.service.CargoInfoService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/rest_cargo")
@CrossOrigin("*")
public class CargoInfoRestController {
    @Autowired
    private CargoInfoService cargoInfoService;

    @GetMapping("cargo_info")
    public ResponseEntity<List<CargoInfo>> getAllCargoInfo(){
        try {
            List<CargoInfo> cargoInfoList = this.cargoInfoService.getAllCargoInfo();
            return new ResponseEntity<>(cargoInfoList, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("cargo_info/{id}")
    public ResponseEntity<?> getCargoInfoById(@PathVariable("id") Long id){
        try {
            CargoInfo cargoInfo = this.cargoInfoService.getCargoInfoById(id);
            return new ResponseEntity<>(cargoInfo, HttpStatus.OK);
        }
        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("cargo_info")
    public ResponseEntity<CargoInfo> saveCargoInfo(@RequestBody CargoInfo cargoInfo){
        try {
            this.cargoInfoService.saveCargoInfo(cargoInfo);
            return new ResponseEntity<CargoInfo>(cargoInfo, HttpStatus.OK);
        }
        catch (Exception exception){
            Map<String, String> result = new HashMap<>();
            result.put("Message", exception.getMessage());
            result.put("Data", null);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("cargo_info/{id}")
    public ResponseEntity<?> updateCargoInfo(@RequestBody CargoInfo cargoInfo, @PathVariable("id") Long id){
        try {
            cargoInfo.setId(id);
            this.cargoInfoService.saveCargoInfo(cargoInfo);
            Map<String, Object> result = new HashMap<>();
            String message = "Success Input Data !!!!!";
            result.put("Message", message);
            result.put("Data", cargoInfo);
            return new ResponseEntity<>(cargoInfo, HttpStatus.OK);
        }
        catch (Exception exception) {
            Map<String, String> result = new HashMap<>();
            result.put("Message", exception.getMessage());
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("cargo_info/{id}")
    public ResponseEntity<?> deleteCargoInfo(@PathVariable("id") Long id){
        try {
            CargoInfo cargoInfo = this.cargoInfoService.getCargoInfoById(id);

            if (cargoInfo != null){
                this.cargoInfoService.deleteCargoInfo(id);
                return ResponseEntity.status(HttpStatus.OK).body("Cargo Info Deleted !!!!!");
            }
            else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Cargo Info Not Found !!!!!");
            }
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
